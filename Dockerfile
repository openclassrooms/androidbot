FROM python:3-alpine

WORKDIR /usr/src/app

RUN apk add --no-cache tzdata
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY androidbot.py ./

CMD [ "python3", "-u", "./androidbot.py" ]