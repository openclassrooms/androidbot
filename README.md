# AndroidBot

Discord bot to welcome new comers to the Android course.  

**Warning! This project is abandoned in favor of [dBot](https://bitbucket.org/openclassrooms/dbot/src/master/)**.  

## Usage

### Build Docker image

```bash
docker build -t imageName .
```

### Configure

Edit your env file `env`:  

```file
DISCORD_TOKEN=Mz0_MyToken_qPv
TZ=Europe/Paris
```

### Run

Start container in detached mode:  

```bash
docker run -d --rm --name containerName --env-file=env imageName
```

## Maintenance

```bash
docker logs containerName
# example:
#2019-11-17 10:49:18 - AndroidBot has connected to Discord
#2019-11-17 13:27:49 - New member James on MyGuild
#2019-11-17 16:28:30 - Member Pierre left MyGuild
```

## Sources

[Discord Python module - GitHub](https://github.com/Rapptz/discord.py)  
[How to make a discord bot - RealPython](https://realpython.com/how-to-make-a-discord-bot-python/)  
[Dockerfile best practices - Docker](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/)  
