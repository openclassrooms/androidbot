#!/usr/bin/python3


import discord

from os import getenv
from datetime import datetime


def console_log(msg):
    print(datetime.now().strftime('%Y-%m-%d %H:%M:%S') + ' - ' + msg)


class AndroidBot(discord.Client):

    async def on_ready(self):
        console_log(self.user.name + ' has connected to Discord')

    async def on_member_join(self, member):
        msg = 'Welcome {0.name} to **{0.guild.name}** !'.format(member)
        if member.guild.system_channel is not None:
            msg += ' Present yourself on <#{0.guild.system_channel.id}>.'.format(member)

        await member.create_dm()
        await member.dm_channel.send(msg)

        console_log('New member {0.name} on {0.guild.name}'.format(member))

    async def on_member_remove(self, member):
        console_log('Member {0.name} left {0.guild.name}'.format(member))


if __name__ == '__main__':
    AndroidBot().run(getenv('DISCORD_TOKEN'))

    exit(0)
